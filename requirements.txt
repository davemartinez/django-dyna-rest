certifi==2018.4.16
Django==1.11
djangorestframework==3.8.2
djangorestframework-jwt==1.11.0
mysqlclient==1.3.12
PyJWT==1.6.3
pytz==2018.4
