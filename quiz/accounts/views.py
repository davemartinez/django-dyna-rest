from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.views.generic import View
from django.shortcuts import render
from django.urls import reverse

from .forms import LoginForm, RegistrationForm


# Create your views here.
class AccountLoginView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('index'))

        form = LoginForm()
        args = {'form': form}
        return render(request, 'accounts/login.html', args)

    def post(self, request):
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/')

        messages.add_message(
            request, 50, 'Wrong login credentials.', extra_tags='danger'
        )
        return render(request, 'accounts/login.html', {'form': form})


class AccountRegisterView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('index'))
            
        form = RegistrationForm()
        args = {'form': form}
        return render(request, 'accounts/register.html', args)

    def post(self, request):
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Successfully Registered. You can now login.'
             )
            return HttpResponseRedirect(reverse('accounts:login'))
        return render(request, 'accounts/register.html', {'form': form})


class AccoutLogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse('accounts:login'))
