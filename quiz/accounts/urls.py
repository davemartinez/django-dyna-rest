from django.conf.urls import url

from . import views


app_name = 'accounts'
urlpatterns = [
    url(r'^login/', views.AccountLoginView.as_view(), name='login'),
    url(r'^register/', views.AccountRegisterView.as_view(), name='register'),
    url(r'^logout/', views.AccoutLogoutView.as_view(), name='logout'),
]
