from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class LoginForm(forms.Form):
        username = forms.CharField(widget=forms.TextInput(attrs={
            'placeholder': 'Enter username',
            'class': 'form-control'
        }))
        password = forms.CharField(widget=forms.TextInput(attrs={
            'placeholder': 'Enter password',
            'class': 'form-control',
            'type': 'password'
        }))


class RegistrationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2']
