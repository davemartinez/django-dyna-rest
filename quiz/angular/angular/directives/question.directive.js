(function() {
    'use strict';

    angular.module('quizApplication')
        .directive('quizApplicationQuestion', QuestionDirective);

    function QuestionDirective() {
        return {
                templateUrl: '/static/angular/directives/question.html',
                restrict: 'E'
        };
    };
})();
