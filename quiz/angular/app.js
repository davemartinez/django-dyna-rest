var app = angular.module(
    'quizApplication', [
        'auth0',
        'ui.router',
        'angular-storage',
        'angular-jwt',
        'simplePagination'
    ]
);

app.config(function($urlRouterProvider, $stateProvider, $locationProvider,
    $httpProvider) {

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    // States
    $stateProvider
        .state('quizList', {
            url: '/',
            templateUrl: 'static/angular/templates/quiz/index.html',
            controller: 'quizListController',
            resolve: {
                quizzes: function($http) {
                    return $http.get('/api/quiz/').then(function(response) {
                        return response.data;
                    })
                }
            }
        })
        .state('quizDetail', {
            url: '^/quiz/{id:[0-9]+}',
            templateUrl: 'static/angular/templates/quiz/detail.html',
            controller: 'quizDetailController',
            resolve: {
                quiz: function($http, $stateParams) {
                    return $http.get('/api/quiz/' + $stateParams.id).then(function(response) {
                        return response.data;
                    }).catch(function() {
                        window.location.href = 'accounts/login/';
                    })
                }
            }
        })
        .state('myQuizList', {
            url: '^/myquizzes/',
            templateUrl: 'static/angular/templates/quiz/my_quiz_list.html',
            controller: 'myQuizListController',
            resolve: {
                quizzes: function($http, $stateParams) {
                    return $http.get('/api/myquizzes/').then(function(response) {
                        return response.data;
                    }).catch(function() {
                        window.location.href = 'accounts/login/';
                    })
                }
            }
        })
        .state('myQuizDetail', {
            url: '^/myquizzes/{id:[0-9]+}',
            templateUrl: 'static/angular/templates/quiz/my_quiz_detail.html',
            controller: 'myQuizDetailController',
            resolve: {
                quiz: function($http, $stateParams) {
                    return $http.get('/api/myquizzes/' + $stateParams.id).then(function(response) {
                        return response.data;
                    }).catch(function() {
                        window.location.href = 'accounts/login/';
                    })
                }
            }
        })
        .state('quizCreate', {
            url: '^/quiz/create/',
            templateUrl: 'static/angular/templates/quiz/quiz_create.html',
            controller: 'quizCreateController',
        })
        .state('startQuiz', {
            url: '^/startquiz/{id:[0-9]+}',
            templateUrl: 'static/angular/templates/quiz/startquiz.html',
            controller: 'startQuizController',
            resolve: {
                attempt: function($http, $stateParams) {
                            return $http.get('/api/attempt/' + $stateParams.id).then(function(response) {
                                return response.data;
                            })
                        },
            }
        })
        .state('results', {
            url: '^/results',
            templateUrl: 'static/angular/templates/accounts/results.html',
            controller: 'dashboardController',
            resolve : {
                attempts: function($http) {
                    return $http.get('/api/attempt/').then(function(response) {
                        return response.data;
                    }, function(reason) {
                        window.location.href = '/accounts/login';
                    });
                }
            }
        })
        .state('resultsDetail', {
            url: '^/results/:id',
            templateUrl: 'static/angular/templates/accounts/results_detail.html',
            controller: 'resultsDetailController',
            resolve : {
                attempt: function($http, $stateParams, $location) {
                    return $http.get('/api/attempt/' + $stateParams.id).then(function(response) {
                        return response.data;
                    }).catch(function(reason) {
                        window.location.href = '/results';
                    });
                }
            }
        });

    // Some configs
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
});

app.controller('resultsDetailController', function($scope, attempt) {
    $scope.attempt = attempt;
    $scope.isQuestionCorrect = isQuestionCorrect;
});

app.controller('dashboardController', function($scope, attempts) {
    $scope.attempts = [];
    attempts.forEach(function(attempt) {
        if(attempt.finished) {
            attempt.score = getScore(attempt);
            $scope.attempts.push(attempt);
        }
    });

});


app.controller('startQuizController', function($scope, $http, Pagination, attempt, $location) {
    if(attempt.finished) {
        $location.path('/results');
    }

    $scope.quiz = attempt.quiz;
    $scope.questions = $scope.quiz.questions;
    $scope.pagination = Pagination.getNew(1);
    $scope.pagination.numPages = Math.ceil($scope.questions.length/$scope.pagination.perPage);
    $scope.answer = {
        answer: '',
        attempt: attempt.id,
        question: ''
    }
    $scope.submitAnswer = function(question, answer) {
        $scope.answer.question = question.id;
        return $http.post('/api/useranswer/', answer).then(function(response) {
            alert("Answer submitted: " + response.data.answer.text);
        }, function(reason) {
            alert("Failed to submit answer. Retry.");
        });
    }

    $scope.endQuiz = function() {
        var sendAttempt = attempt;
        sendAttempt.finished = true;
        return $http.put('/api/attempt/' + sendAttempt.id, sendAttempt)
            .then(function(response) {
                alert('Finished');
                $location.path('/results');
            }, function(reason) {
                alert('Failed to end quiz. Try again.');
            });
    }
});

app.controller('quizListController', function($scope, quizzes, Pagination) {
    $scope.quizzes = quizzes;
    $scope.pagination = Pagination.getNew(5);
    $scope.pagination.numPages = Math.ceil($scope.quizzes.length/$scope.pagination.perPage);
});

app.controller('quizDetailController', function($scope, $location, $http, quiz) {
    $scope.quiz = quiz;
    var attempt = {
        quiz: quiz.id
    }

    $scope.startQuiz = function() {
        return $http.post('/api/attempt/', attempt)
                .then(function(response) {
                    attempt = response.data;
                    $location.path('/startquiz/' + attempt.id);
                }, function() {
                    alert("You must login first to start quiz. Redirecting.");
                    window.location.href = '/accounts/login/';
                });
    };
});

app.controller('myQuizListController', function($scope, quizzes, Pagination) {
    $scope.quizzes = quizzes;
    $scope.pagination = Pagination.getNew(5);
    $scope.pagination.numPages = Math.ceil($scope.quizzes.length/$scope.pagination.perPage);
});

app.controller('myQuizDetailController', function($scope, $http, $location, quiz) {
    $scope.quiz = quiz;

    $scope.saveQuizEdit = function() {
        var quiz = {
            quiz: $scope.quiz.id,
            title: $scope.quiz.title
        }
        $http.put('/api/quiz/' + $scope.quiz.id, quiz)
            .then(function(response) {
                $scope.quiz.title = response.data.title;
            }).catch(function() {
                alert("Unable to update quiz.");
            });
    }

    $scope.addQuestion = function() {
        var question = {
            quiz: quiz.id,
            text: "<Blank Question>"
        }
        $http.post('/api/question/', question)
            .then(function(response) {
                $scope.quiz.questions.push(response.data);
            }).catch(function() {
                alert("Unable to create question.");
            });
    }

    $scope.addChoice = function(question, choiceText) {
        var choice = {
            question: question.id,
            text: choiceText
        }
        $http.post('/api/choice/', choice)
            .then(function(response) {
                question.choices.push(response.data);
            }).catch(function() {
                alert("Unable to create choice.");
            });
    };

    $scope.deleteQuiz = function(quiz) {
        $http.delete('/api/quiz/' + quiz.id)
            .then(function(response) {
                $location.url('/myquizzes/');
            });
    };

    $scope.updateChoice = function(choice) {
        $http.put('/api/choice/' + choice.id, choice)
            .catch(function() {
                alert("Unable to update choice");
            });
    }

    $scope.deleteChoice = function(question, choice) {
        $http.delete('api/choice/' + choice.id)
            .then(function(response) {
                var index = question.choices.indexOf(choice);
                question.choices.splice(index, 1);
            });
    };

    $scope.deleteQuestion = function(question) {
        $http.delete('api/question/' + question.id)
            .then(function(response) {
                var index = $scope.quiz.questions.indexOf(question);
                $scope.quiz.questions.splice(index, 1);
            });
    };
    $scope.saveQuestionEdit = function(input) {
        var question = {
            id: input.id,
            text: input.text,
            quiz: input.quiz
        };
        $http.put('/api/question/' + question.id, question)
            .catch(function() {
                alert("Unable to update question");
            });
    };
});

app.controller('quizCreateController', function($scope, $http, $location) {
    $scope.create = function() {
        $http.post('/api/quiz/', {title: $scope.quizTitle});
        var url =  $location.url('/myquizzes/');
    };
});
