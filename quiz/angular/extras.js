function getScore(attempt) {
    score = 0;
    var questions = attempt.quiz.questions;
    questions.forEach(function(question) {
        if(isQuestionCorrect(attempt, question)) {
            score++;
        }
    });
    return score;
}

var isQuestionCorrect = function(attempt, question) {
    var answerSet = new Set();
    var choiceSet = new Set();
    question.answers.forEach(function(answer) {
        if(answer.attempt == attempt.id) {
            answerSet.add(answer.answer.text);
        }
    });
    question.choices.forEach(function(choice) {
        if(choice.correct) {
            choiceSet.add(choice.text);
        }
    });
    if(setsEqual(answerSet, choiceSet)) {
        return true;
    }
    return false;
}


function setsEqual(a,b){
    if (a.size !== b.size)
        return false;
    let aa = Array.from(a);
    let bb = Array.from(b);
    return aa.filter(function(i){return bb.indexOf(i)<0}).length==0;
}
