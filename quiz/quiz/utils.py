def is_question_correct(attempt, question):
    """Returns a boolean value if the question is correct on the take
    Takes a Take(Quiz Session) and the question to check
    """

    choices = question.choices.filter(correct__exact=True)
    answers = question.answer_set.filter(
        attempt__user__exact=attempt.user,
        attempt__exact=attempt
    )
    set_choices = set([choice.choice_text for choice in choices])
    set_answers = set(
        [answer.selected_choice.choice_text for answer in answers]
    )
    return set_choices == set_answers
