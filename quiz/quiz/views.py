from rest_framework import generics
from rest_framework import permissions
from django.shortcuts import get_object_or_404
from django.utils import timezone

from .models import Quiz, Question, Choice, Attempt, UserAnswer
from .serializers import QuizSerializer, QuestionSerializer, ChoiceSerializer, AttemptSerializer, UserAnswerSerializer
from .permissions import IsTakenByAuthenticatedUser, IsOwner


class QuizList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    serializer_class = QuizSerializer

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def get_queryset(self):
        return Quiz.objects.filter(expiration_date__gte=timezone.now())


class QuizDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsOwner,)
    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer


class QuestionList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class QuestionDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class ChoiceList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer


class ChoiceDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer


class MyQuizList(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = QuizSerializer

    def get_queryset(self):
        return Quiz.objects.filter(created_by__exact=self.request.user)


class MyQuizDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated, IsOwner)
    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer


class MyResultList(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = QuizSerializer

    def get_queryset(self):
        return Quiz.objects.filter(created_by__exact=self.request.user)


class MyResultDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer


class AttemptList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Attempt.objects.all()
    serializer_class = AttemptSerializer

    def perform_create(self, serializer):
        quiz = get_object_or_404(Quiz, pk=self.request.data.get('quiz'))
        if quiz.expiration_date >= timezone.now():
            serializer.save(user=self.request.user, quiz=quiz)

    def get_queryset(self):
        return Attempt.objects.filter(user__exact=self.request.user)


class AttemptDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsTakenByAuthenticatedUser)
    queryset = Attempt.objects.all()
    serializer_class = AttemptSerializer


class UserAnswerList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = UserAnswer.objects.all()
    serializer_class = UserAnswerSerializer

    def perform_create(self, serializer):
        answer = get_object_or_404(Choice, pk=self.request.data.get('answer'))
        question = get_object_or_404(Question, pk=self.request.data.get('question'))
        serializer.save(answer=answer, question=question)


class UserAnswerDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = UserAnswer.objects.all()
    serializer_class = UserAnswerSerializer
