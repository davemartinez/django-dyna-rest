from rest_framework import permissions


class IsTakenByAuthenticatedUser(permissions.BasePermission):
    message = "User did not take this quiz"

    def has_object_permission(self, request, view, object):
        return request.user == object.user


class IsOwner(permissions.BasePermission):
    message = "You do not own this quiz"

    def has_object_permission(self, request, view, object):
        return request.user == object.created_by
