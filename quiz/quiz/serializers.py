from rest_framework import serializers
from django.contrib.auth.models import User

from .models import Quiz, Question, Choice, Attempt, UserAnswer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = (
            'id', 'text', 'question', 'correct'
        )


class UserAnswerSerializer(serializers.ModelSerializer):
    answer = ChoiceSerializer(many=False, read_only=True)

    class Meta:
        model = UserAnswer
        fields = (
                'id', 'attempt', 'answer'
        )


class QuestionSerializer(serializers.ModelSerializer):
    choices = ChoiceSerializer(many=True, read_only=True)
    answers = UserAnswerSerializer(many=True, read_only=True)

    class Meta:
        model = Question
        fields = (
                'id', 'text', 'quiz', 'choices', 'answers'
        )


class QuizSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True, read_only=True)
    created_by = UserSerializer(many=False, read_only=True)

    class Meta:
        model = Quiz
        fields = (
            'id', 'created_by', 'title', 'date_created', 'expiration_date',
            'questions'
        )
        read_only_fields = ('created_by', 'date_created', 'expiration_date')


class AttemptSerializer(serializers.ModelSerializer):
    quiz = QuizSerializer(many=False, read_only=True)

    class Meta:
        model = Attempt
        fields = (
                'id', 'quiz', 'user', 'date_taken', 'score', 'finished'
        )
        read_only_fields = ('user',)
