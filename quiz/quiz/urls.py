"""quiz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView

from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token

from . import views


urlpatterns = [
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^api/quiz/$', views.QuizList.as_view()),
    url(r'^api/quiz/(?P<pk>[0-9]+)$', views.QuizDetail.as_view()),
    url(r'^api/myquizzes/$', views.MyQuizList.as_view()),
    url(r'^api/myquizzes/(?P<pk>[0-9]+)$', views.MyQuizDetail.as_view()),
    url(r'^api/myresults/$', views.MyResultList.as_view()),
    url(r'^api/myresults/(?P<pk>[0-9]+)$', views.MyResultDetail.as_view()),
    url(r'^api/question/$', views.QuestionList.as_view()),
    url(r'^api/question/(?P<pk>[0-9]+)$', views.QuestionDetail.as_view(), name='question-detail'),
    url(r'^api/choice/$', views.ChoiceList.as_view()),
    url(r'^api/choice/(?P<pk>[0-9]+)$', views.ChoiceDetail.as_view(), name='choice-detail'),
    url(r'^api/attempt/$', views.AttemptList.as_view()),
    url(r'^api/attempt/(?P<pk>[0-9]+)$', views.AttemptDetail.as_view()),
    url(r'^api/useranswer/$', views.UserAnswerList.as_view()),
    url(r'^api/useranswer/(?P<pk>[0-9]+)$', views.UserAnswerDetail.as_view()),
    url(r'^', TemplateView.as_view(template_name='quiz/index.html'), name='index'),
]
