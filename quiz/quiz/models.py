import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from .utils import is_question_correct


def timezone_now_plus_one_day():
    return timezone.now() + datetime.timedelta(days=1)


class Quiz(models.Model):
    created_by = models.ForeignKey(User)
    date_created = models.DateTimeField(default=timezone.now)
    expiration_date = models.DateTimeField(default=timezone_now_plus_one_day)
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "quizzes"


class Question(models.Model):
    quiz = models.ForeignKey(Quiz, related_name='questions', on_delete=models.CASCADE)
    text = models.CharField(max_length=200)

    def __str__(self):
        return self.text


class Choice(models.Model):
    question = models.ForeignKey(Question, related_name='choices', on_delete=models.CASCADE)
    text = models.CharField(max_length=200)
    correct = models.BooleanField(default=False)

    def __str__(self):
        return self.text


class Attempt(models.Model):
    quiz = models.ForeignKey(Quiz)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_taken = models.DateTimeField(default=timezone.now)
    finished = models.BooleanField(default=False)

    def __str__(self):
        return str(self.user) + " : " + str(self.quiz)

    def get_score(self):
        questions = self.quiz.question_set.all()
        score = 0
        for question in questions:
            if is_question_correct(self, question):
                score = score + 1

        return "%d out of %d" % (score, questions.count())

    score = property(get_score)


class UserAnswer(models.Model):
    attempt = models.ForeignKey(Attempt, on_delete=models.CASCADE)
    answer = models.ForeignKey(Choice, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, related_name="answers")

    def __str__(self):
        return self.answer.text
